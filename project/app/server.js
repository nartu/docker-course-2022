let express = require('express');
let path = require('path');
let fs = require('fs');
let MongoClient = require('mongodb').MongoClient;
let bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.sendFile(path.join(__dirname, "index.html"));
  });

app.get('/profile-picture', function (req, res) {
  let img = fs.readFileSync(path.join(__dirname, "images/profile-1.jpg"));
  res.writeHead(200, {'Content-Type': 'image/jpg' });
  res.end(img, 'binary');
});

// use when starting application locally
let mongoUrlLocal = "mongodb://admin:abc@localhost:27017";

// use when starting application as docker container
let mongoUrlDocker = "mongodb://admin:abcd@mongodb:27017";

// use when deploy
const user = process.env.MONGO_USER ? process.env.MONGO_USER : 'admin';
const password = process.env.MONGO_PASSWORD ? process.env.MONGO_PASSWORD : '';
const server = process.env.MONGO_SERVER ? process.env.MONGO_SERVER : 'db';
let mongoUrlDockerDeploy = `mongodb://${user}:${password}@${server}:27017`;

let connectString = mongoUrlDockerDeploy;

// pass these options to mongo client connect request to avoid DeprecationWarning for current Server Discovery and Monitoring engine
let mongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true };

// "user-account" in demo with docker. "my-db" in demo with docker-compose
let databaseName = "my-db";

app.post('/update-profile', function (req, res) {
  // res.header("Access-Control-Allow-Origin", "*");

  let userObj = req.body;

  MongoClient.connect(connectString, mongoClientOptions, function (err, client) {
    if (err) throw err;

    let db = client.db(databaseName);
    userObj['userid'] = 1;

    let myquery = { userid: 1 };
    let newvalues = { $set: userObj };

    db.collection("users").updateOne(myquery, newvalues, {upsert: true}, function(err, res) {
      if (err) throw err;
      client.close();
    });

  });
  // Send response
  res.send(userObj);
});

app.get('/get-profile', function (req, res) {
  // res.header("Access-Control-Allow-Origin", "*");
  let response = {};
  // Connect to the db
  MongoClient.connect(connectString, mongoClientOptions, function (err, client) {
    if (err) throw err;

    let db = client.db(databaseName);

    let myquery = { userid: 1 };

    db.collection("users").findOne(myquery, function (err, result) {
      if (err) throw err;
      response = result;
      client.close();

      // Send response
      res.send(response ? response : {});
    });
  });
});

app.get('/version', (req, res) => {
  const v = process.version
  res.send('Привет! '+'Version nodeJS: ' + v)
})

app.get('/env', (req, res) => {
  const response = process.env
  res.send(response ? response : {});
  // res.send(response.MONGO_PASSWORD);
})

app.listen(3000, function () {
  console.log("app listening on port 3000!");
});
