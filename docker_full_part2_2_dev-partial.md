## Использование Docker в процессе разработки

1. Удобно когда код, текстовый редактор и интерпретатор языка программирования находятся на одной машине. Поэтому в процессе разработки код запускается с локальной машины. В нашем случае используется [NodeJS](https://nodejs.org/en/), он установлен на компьютере.

2. `npm i` устанавливает недостающие пакеты, они прописаны в файле `package.json`. Запустить скрипт `node server.js` или можно ещё `npm start server.js`, но горячая перегрузка здесь не поддерживается, т.е нужно каждый раз останавливать node и запускать снова, чтобы увидеть изменения в коде. Для того чтобы изменения сразу были доступны используются специальные плагины, например nodemon `npx nodemon server.js`. `npx` запускает локальный модуль так, как будто он установлен глобально (т.е не нужно прописывать пути), если модуль не установлен, `npx` предлагает его загрузить, но автоматически не включает в список. Нужно явно согласиться либо поставить `--yes or -y` и загрузка будет автоматической `npx nodemon -y server.js`. Приложение будет работать на http://localhost:3000

3. А вот базу данных удобнее запустить в Docker. Нам понадобится [mongo](https://hub.docker.com/_/mongo). Если клиент и БД запущены на одной машине, подключение возможно через unix socket без имени пользователя БД и пароля. Но обычно БД находятся на другом сервере, и нужно указать имя и пароль администратора ДБ, по ним мы будем подключаться. В Docker их добавление делается с помощью указания специальных переменных окружения. Эти переменные окружения добавлены создателями образа и написаны в документации, стартовый скрипт контейнера возьмёт их и добавит в БД. В Docker они прописываются с помощью `--env or -e` и пары ключ=значение `docker run -e ENV=value IMAGE`. В linux посмотреть все переменные окружения и их значения можно командой `env`.

4. Чтобы задать имя контейнера явно испозуют `docker run --name any-word IMAGE`, переименовать существующий контейнер `docker rename CONTAINER new_name`

5. Запускаем mongo командой, привязывает БД к порту на хосте, задаём имя контейнера и задаём имя и пароль администратора БД.
  ```
  docker run -d \
  -p 27017:27017 \
  --name db \
  -e MONGO_INITDB_ROOT_USERNAME=admin \
  -e MONGO_INITDB_ROOT_PASSWORD=abc \
  mongo
  ```
6. Теперь строчка подключения к ДБ выглядит так `mongodb://admin:abc@localhost:27017`, можно зайти в базу с консольного клиента `mongosh "mongodb://admin:abc@localhost:27017"`, в проекте надо убедиться что `let connectString = mongoUrlLocal`

7. Подключим [mongo-express](https://hub.docker.com/_/mongo-express) клиент для mongo с графическим интерфейсом. Ему надо указать следующие параметры подключения: сервер БД, порт (если 27017 можно опустить), имя администратора, пароль администратора. Сервер должен быть не localhost (и вообще у каждого контейнера свой localhost). Лучшим решением будет поместить контейнеры mongo и mongo-express в одну виртуальную сеть Docker, и тогда эти контейнеры могут видеть друг друга по имени контейнера, это доменное имя в рамках этой локальной сети. В виртуальной сети по умолчанию у контейнеров нет доменных имён.

8. Создать пользовательскую виртуальную сеть нужно командой `docker network create name-of-the-network`, посмотреть доступные сети `docker network ls`.

9. Добавляет контейнер с mongo к сети `docker network connect NETWORK CONTAINER`, при этом контейнер будет сразу в двух сетях: сети по умолчанию bridge и нашей новой сети. Посмотреть в каких сетях контейнер можно командой `docker inspect CONTAINER` в разделе Networks, а какие контейнеры в сети командой `docker inspect NETWORK`

10. Подключить контейнер к сети при запуске можно с помощью `--network or --net` `docker run --net NETWORK IMAGE`

11. Подключаем mongo-express. После этого можно заходить на сайт http://localhost:8081 и видеть БД более удобно, в качестве сервера подключения к БД указываем имя контейнера с mongo т.к оба контейнера будут в одной сети и могут видеть друг друга по имени контейнера
  ```
  docker run -d \
  -p 8081:8081 \
  --net NETWORK \
  --name mex \
  -e ME_CONFIG_MONGODB_SERVER=db \
  -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
  -e ME_CONFIG_MONGODB_ADMINPASSWORD=abc
  mongo-express
  ```
12. Итак, наше приложение подключается в БД по localhost, а mongo-express подключается к ДБ по имени контейнера по сети, в этом подключении необязательно публиковать БД, она может быть недоступна из вне и связь с ней только через другие приложения. Но пока пока можно подключаться ко всем трём сервисам по localhost.
