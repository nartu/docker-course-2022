## Docker-compose для разработки

1. Для автоматизации запуска контейнеров используется Docker-compose. Docker-compose читает файл в формате yaml и выполняет действия описанные в нём `docker-compose --file myfile.[yml|yaml] up [--detach]` или можно просто `docker-compose up -d` если файл называется docker-compose.yml, `-d` чтобы не замораживать окно терминала, фоновый режим.

2. Удалить запущенные с помощью docker-compose контейнеры и сети (если они не существовали до этого). `docker-compose --file myfile.[yml|yaml] down [--detach]`. Созданные volume остаются.

3. У файла для docker-compose есть корневые секции version: services: volumes: networks: и другие. Минимальная структура файла выклядит так:
  ```
  version: '3.9'
  # Комментарий

  services:
    service_name:
      image: IMAGE
  ```
  Это тоже самое что запустить в командной строке `docker run IMAGE`. Имя контейнера будет `проект_сервис_1`, проект это имя директории в которой файл. В yml файлах важны отступы!

4. Запускаем БД mongo через yml скрипт. В корневой директории проекта создадим файл docker-compose.yml и запишем там:
  ```
  version: '3'

  services:
    mongodb:
      # container_name: db
      image: mongo
      ports:
        - 27020:27017
      environment:
        - MONGO_INITDB_ROOT_USERNAME=admin
        - MONGO_INITDB_ROOT_PASSWORD=abc
      # volumes:
        # - mongo_data:/data/db
  ```
  Docker создаст для нас контейнер с названием app_mongodb_1, а так же виртуальную сеть типа bridge с названием app_default и подключит наш контейнер к этой сети.

5. Теперь подключим mongo-express, добавим в файл ещё один сервис.
```
mongo-express:
  image: mongo-express
  restart: always # fixes MongoNetworkError when mongodb is not ready when mongo-express starts
  ports:
    - 8080:8081
  environment:
    - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
    - ME_CONFIG_MONGODB_ADMINPASSWORD=abc
    - ME_CONFIG_MONGODB_SERVER=mongodb
  depends_on:
    - mongodb
```

6. Для того, чтобы не прописывать пароли в файл, можно указывать переменные окружения. Для этого надо предварительно создать переменную окружения в системе `export MONGO_PASS="abc"` и вместо пароля подставить `$MONGO_PASS` в обоих сервисах. На машине, где скрипт будет разворачиваться такая переменная окружения тоже должна существовать.

7. Для того чтобы создать том, надо добавить корневую секцию `volume:`, если такой уже существует, он будет использован.
  ```
  volumes:
    mongo_data:
      name: mongo_data
  ```
  В сервисе надо добавить:
  ```
  volumes:
    - mongo_data:/data/db
  ```

8. Для того чтобы поднять сервис (контейнер) через который можно работать (писать код и смотреть результат в браузере) в секции `services:` дописываем
  ```
  app-dev:
    image: node
    restart: always
    expose:
      - 3000
    ports:
      - 3030:3000
    working_dir: /home/node/app
    volumes:
      - .:/home/node/app
    command: bash -c "npm i && npx -y nodemon server.js"
    depends_on:
      - mongodb
  ```

9. Документация по docker-compose https://docs.docker.com/compose/compose-file/
