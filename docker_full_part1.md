## Докер начало

1. Docker - это средство виртуализации на уровне операционной системы (контейнерой виртуализации), позволяющее иметь несколько независимых пространств пользователей (контейнеров) на хост машине. Каждый контейнер имеет собственный IP в локальной подсети и независимый набор программного обеспечения, и по своему функционалу похож на локальную машину. Докер не приложение с подобным функционалом, существуют аналоги: OpenVZ, Virtuozzo Containers, Solaris Containers, FreeBSD Jail, [LXC](https://linuxcontainers.org/lxc/introduction/#LXCFS) (изначально Docker разрабатывался на его основе). Докер - самый молодой из них, и благодаря удобству управления, кроссплатформенноти, собственной облачной библиотеке образов (Docker Hub) и работа с которой проста и настроена из коробки, является самым популярным инструментом контейнерной виртуализации и широко используется не только системными администраторами, но и разработчиками.
В настоящее время, базовое знание Docker включено в требуемые навыки большинства IT вакансий.

2. Классическая схема работы - это установка на компьютер разработчика всех нужных библиотек, программ, переменных виртуального окружения. На боевом сервере тоже установлены эти зависимости, переменные виртуального окружения. И если что-то работает на одной машине, далеко не факт что точно так же работает на другой. И обновления - это всегда опасная и сложная процедура, ибо достичь полной идентичности непросто, а малейшие различия могут приводить к ошибкам.  Переносить такую архитектуру с сервера на сервер тоже непросто. Докер решает эту проблему упаковывая каждое приложения со всеми зависимостями и окружением и конфигурациями в отдельный контейнер, который может быть развёрнут на разных linux-системах, и работать совершенно одинаково. Т.е у нас получается не одна машина-комбаин, а целая сеть взаимодействующих между сабой машин, которые физичеки живут на машине-хосте, и управляются из неё набором команд. При этом архитектура может быть описана как исполняемый код (yaml).

3. Но всё-таки контейнеры не являются полноценными виртуальными машинами, т.к они работают разных уровнях. Любой компьютер вместе с операционной системой можно разделить на три уровня (слоя): аппаратный, Kernel OS, программный. Ядро непосредственно взаимодействует с физическим оборудованием. Программное обеспечение запускается поверх ядра. Контейнер - это уровень ядра ОС: разное программное обеспечение, но одно ядро (ядро хоста). VM - это аппаратный уровень. Специальные программы гипервизоры (например VirtualBox или VMware Workstation) позоляют расшаривать физческое аппаратное обеспечение, и поверх него устанавливать ядра разных операционных систем независимо друг от друга. Средства контейнеризации, в том числе докер, работает поверх ядра ОС хоста, и позволяет расшаривать её ядро на независимые контейнеры. Контейнеры могут иметь только ОС хоста, но они занимаются немного места, берут на себя минимум ресурсов (не надо специально выделять) и легко управляются. ВМ - могут иметь отличную от хоста ОС, занимают много места, берут много ресурсов, дольше запускаются и сложнее управляются.
Докер движок работает на linux. Поэтому контейнеры могут быть только с ядром Linux, но программный уровень у каждого свой, в том числе могут быть разные ОС основанные на ядре linux: Ubuntu, Debian, Centos, Fedora и другие.
Виртуализация может быть сразу на двух уровнях. Например, на машине с Windows может быть ВМ с linux, а уже на ней докер. Именно так и происходит в случае когда Докер устанавливается на Windows или Mac, в случае с Windows это WSL, в случае с Mac отдельная ВМ, но она устанавливается автоматически и незаметная для пользователя.

4. У Докер есть две основные сущности: контейнер и образ. Образ это шаблон, или можно сказать live cd (аналог Virtual Hard Drive файла для виртуальных машин). А контейнер это запущенный из этого live cd экземпляр. Т.е контейнер - это живая сущность, которая имеет конечный срок жизни и с которой можно взаимодействовать, а образ - это его ДНК. Экземпляров, т.е контейнеров, из одного образа может быть сколько угодно, некоторым из них могут получить дополнительные параметры при запуске, например значения переменные окружения, добавлена связь с хостом, назначено общее с хост системой дисковое пространство (директория).
<!-- Или можно сказать, что образ это ДНК, а контейнер существо из этого ДНК, с которым мы имеем дело, оно рождается и умирает, а так же может быть в спячке и поставлено на паузу. -->
Образы бывают базовые и дочерние. Базовые создаются в основном самим докером и это образы разных операционных систем. Дочерние строятся на основе других образов. Каждый образ состоит из слоёв, первый слой - это образ на основе которого создан этот, дальше идут пользовательские слои, каждое изменение это новый слой. Слои используются многократно, хранятся в одном месте (в рамках одного репозитория в хранилище или локально), за счёт такой системы экономится место и время на создание и развёртывание. Докер использует объединённую файловую систему, в основном [overlay2](https://docs.docker.com/storage/storagedriver/select-storage-driver/), принцип работы которой похож на принцип систем контроля версий. Слои образа неизменяемы, изменения могут быть только за счёт добавления новых слоёв в образ с помощью определённых команд, либо зафиксированы во время работы контейнера. Дело в том, что работающий контейнер имеет все слои образа и они неизменяемы и изменяемый слой. После удаления контейнера все изменения исчезают навсегда, либо могут быть записаны в новый образ. Так же можно примонтировать volume: т.е хост и контейнер могут иметь общее пространство и данные сохраняются на хост-машине. Об этом подробнее позже.

5. Установка Docker Engine и docker-compose.
Инструкции для всех ОС https://docs.docker.com/engine/install/

  a. Ubuntu
  Устанавливаем Docker и Docker Compose.
  Docker Compose это утилита для автоматизации запуска многоконтейнерных приложений
  - https://ubuntu.com/download/server (его можно установить на VirtualBox)
  - https://docs.docker.com/engine/install/ubuntu/
  - https://docs.docker.com/compose/install/

  b. Windows
  - https://docs.docker.com/desktop/windows/install/#wsl-2-backend
  - https://docs.microsoft.com/en-us/windows/wsl/install
  - https://docs.microsoft.com/en-us/windows/wsl/setup/environment#set-up-your-linux-username-and-password
  - https://docs.docker.com/desktop/windows/install/
  - https://docs.docker.com/desktop/windows/

  c. Mac Просто скачать приложение docker.dmg, если Apple chip предварительно выполнить команду `softwareupdate --install-rosetta`
  - https://docs.docker.com/desktop/mac/install/
