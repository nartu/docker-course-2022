## Docker-compose для деплоя

1. Когда программа написана, её нужно упаковать в образ, чтобы потом из него развернуть контейнер. Текст программы будет скопирован в образ. Для создания образов пишут специальные скрипты [Dockerfile](https://docs.docker.com/engine/reference/builder/#env)

2. Собрать образ из Dockerfile `docker build --file(-f) Custom.Dockerfile --tag(t) image-name .`, . - это текущая директория где искать файл, название по умолчанию Dockerfile.

3. Сделаем образ с нашим nodejs приложением, создаём файл с названием Dockerfile и пишем, за основу лучше взять node:alpine на основе урезанного linux чтобы образ весил меньше, в закрытых облачных докер хранилищах часто платят за место
```
FROM node:alpine
EXPOSE 3000
WORKDIR /home/node/app
COPY ./app .
ENV MONGO_USERNAME=admin \
    MONGO_PASSWORD='' \
    MONGO_SERVER=mongodb
RUN npm install
CMD ["node", "server.js"]
```
4. Собираем образ `docker build -t node_app:v1 -t node_app:latest .`

5. Удобно создать отдельный yaml файл для деплоя `docker-stack.yml`, это копия `docker-compose.yml`, здесь будем менять сервис app-dev, назовём его просто app. В элемент image пишем наш созданный образ node_app:v1 и убираем expose, working_dir, volumes, command. 80 - стандартный порт, можно написать любой другой если этот занят.
```
app:
  image: node_app:latest
  ports:
    - 80:3000
  environment:
    - MONGO_SERVER=mongodb
    - MONGO_USER=admin
    - MONGO_PASSWORD=$MONGO_PASS
  depends_on:
    - mongodb
```
6. Если сборку нужно автоматизировать (например, постоянно что-то меняется в скриптах) можно добавить элемент build в yaml файл.
```
app:
  image: image-name
  build:
    context: .
```

7. У сервиса mongodb убираем раздел ports т.к публиковать БД не нужно, обращение к ней будет по внутренней сети.

8. Если нам нужен доступ к БД через mongo-express нужно добавить логин и пароль для входа, для этого прописываем переменные окружения:
```
  mongo-express:
    ...
    environment:
      ...
      - ME_CONFIG_BASICAUTH_USERNAME=me
      - ME_CONFIG_BASICAUTH_PASSWORD=123
```

9. Проверяем скрипт `docker-compose -f docker-stack.yml up -d`, наш сайт должен работать по адресу http://localhost и mongo-express http://localhost:8080
