## Docker теория и практика

0. В этом видео разберём основные команды Docker, покажем на примере как Docker используется в разработке, и покажем что такое Docker network. Общий полный вид команд docker выглядит так: `docker <называние сущности с которой работаем> команда --параметр аргумент СУЩНОСТЬ [команды внутри контейнера]` Часто используемые параметры имеют алиасы и обычно используют их. Часто алиас это просто команда без имени сущности с которой работаем, но не всегда. Например команды для просмотра списка контейнеров и образов соответственно выглядят так:
`docker container ls` = `docker ps` , `docker image ls` = `docker images`

1. Мы уже использовали эти команды ранее, когда говорили о слоях. `docker pull` просто загружает образ на локальный диск из репозитория. Если такой образ уже существует и он устарел, он обновится. Уникальный адрес образа это `хранилище/репозиторий:тэг`. Хранилище по умолчанию это `docker.io` (docker hub), тэг по умолчанию это это `latest`. Т.е минимально нужно указать репозиторий. Репозиторий состоит из двух слов `username/repo`, username - имя создателя, если не указано, подставляется `library` (в рамках docker hub). Докер сравнивает локальный образ с тем что в хранилище и подгружает недостающие слои, если таковые имеются. Поэтому данная команда не только подгружает, но и приводить образ к актуальному состоянию, чаще всего это нужно с тэгом latest, образы под которым меняются по мере возникновения новых версий дистрибутивов.

2. `docker run` запускает контейнер на основе данного образа. И сначала смотрит образ локально по имени. Если локально образ с таким именем существует, то запускается из него, если нет, предварительно вызывается команда `docker pull`. Но локальный образ может быть не актуальным, и если нужно синхронизировать с хранилищем, стоит запустить `docker pull` предварительно вручную. Так же, на будущее, следует быть осторожными с названиями самодельных образов, не нужно называть их именами популярных продуктов, т.к может возникнуть путаница, Docker всё равно что запускать локально и что обновлять, он ориентируется по названию. Стандартное название пользовательского образа это `username/repository:tag`

3. С помощью команды `docker tag` можно добавить новое название образу.
`docker tag <OLD_REPOSITORY_NAME:TAG or IMAGE_ID> NEW_REPOSITORY_NAME:TAG`

4. Для того чтобы контейнеры делали то что нам нужно, у `docker run` есть множество параметров. Самый важный из них это привязка порта, на котором контейнер слушает к порту на локальной машине `--publish or -p`. Т.е мы можем сделать имитацию работы сервисов на локальной машине.
`docker run -p HOST_PORT:CONTAINER_PORT IMAGE`

5. Так же можно привязать конкретный IP адрес и протокол (по умолчанию tcp).
`docker run -p "HOST_IP:HOST_PORT:CONTAINER_PORT IMAGE/PROTOCOL" IMAGE`

6. Можно сделать привязку портов автоматически `--publish-all or -P`, не нужно указывать ни порт хоста, ни порт контейнера. Но при каждом запуске контейнера после остановки порт будет меняться и порт назначается после 49150.
`docker run -P IMAGE`

7. Команды `docker stop` и `docker start` останавливает и вновь запускает работающий контейнер. Т.е как бы замораживает на время. `docker start` не выполняет начальные скрипты, а только даёт команду продолжать работу. Но останавливает и запускает работающие в контейнере процессы.

8. Выполнить команду в контейнере и получить результат, не интерактивно.
`docker exec CONTAINER some-linux-command args`

9. Зайти внутрь контейнера можно с параметрами `--interactive --tty or -it`
`docker exec -it CONTAINER bash[sh]`

10. Посмотреть логи запущенной в контейнере программы `docker logs CONTAINER`

11. Посмотреть N последних записей `--tail or -n` `docker logs -n N CONTAINER`

12. Добавить в логи дату и время от докер `--timestamps or -t` `docker logs -t CONTAINER`

13. Показ логов в реальном времени, так же как запуск без `--detach` (п. 14), но можно спокойно прерывать и контейнер не остановится `--follow or -f` `docker logs -f CONTAINER`

14. Когда контейнер запускается, окно терминала замораживается и показываются логи в реальном времени (для серверов и т.д, где скрипт выполняется вечно), если послать сигнал остановки Ctrl+C, контейнер тоже остановится, поэтому нужно открывать новое окно терминала. Чтобы контейнер не показывал логи, нужно запускать в фоновом режиме `--detach or -d`.

15. Узнать всё про любую сущность докер (контейнер, образ, сеть, том) `docker container/image/network/volume inspect CONTAINER/IMAGE/NETWORK/VOLUME`. Если нет конфликта имён, можно сокращённо `docker inspect CONTAINER/IMAGE/NETWORK/VOLUME`. Ответ в json-формате.

16. Отформатировать ответ `docker inspect` с помощью параметра `--format - или -f`
`docker inspect -f "Text {{.Root.Tree}}"` (текст вне фигурных скобок может быть любым и в выводе он будет как есть)

17. Посмотреть все работающие контейнеры (статус running) `docker ps`

18. Посмотреть все контейнеры (любой статус)  `docker ps --all` или `docker ps -a`

19. Отформатировать вывод списка контейнеров `docker ps -a --format "table {{.COLUMN}}"` Слово table здесь означает показывать названия столбцов, столбцов можно указывать несколько и нужно явно писать разделитель, например \t. Аналогично в `docker images`.

20. Показать только определённые контейнеры `--filter - или -f` `docker ps --filter "foo=bar"` , например status=exited (контейнеры были запущены, но уже остановились)

21. Посмотреть все имеющиеся образы `docker images`

22. Показать только определённые образы `--filter or -f` `docker images --filter "foo=bar"`, например --filter "dangling=true" покажет все образы с названием <none>:<none>

23. Удалить контейнер `docker container rm CONTAINER` или алиас `docker rm CONTAINER`

24. Удалить работающий (в статусе Running) контейнер `docker stop CONTAINER && docker rm CONTAINER` или форсированно `--force or -f` `docker rm -f CONTAINER`

25. Удалить все контейнеры кроме работающих (в статусе Running) `docker container prune`

26. Можно подставить сразу несколько названий либо ID контейнеров, чтобы получить вывод только ID используется параметр `--quiet or -q` `docker ps -q`, ID всех контейнеров `docker ps -aq`.

27. Аналогично со списком образов, ID всех образов `docker images -aq`, где `--all or -a` - это показывать так же intermediate images, образы (слои) в процессе формирования (выполнения инструкции RUN для `docker build`), это можно опустить.

28. Удалить все контейнеры с любым статусом без подтверждения `docker rm -f $(docker ps -aq)`, так же можно подставить в конструкцию $() любую фильтрацию и написать `-q`

29. Удалить образ `docker image rm IMAGE` или алиас `docker rmi IMAGE`, если из этого образа запущен контейнер и он в статусе Running, образ не удалится.

30. Удалить образ из которого запущен работающий (в статусе Running) контейнер `docker stop CONTAINER && docker rmi IMAGE` или форсированно `--force or -f` `docker rmi -f IMAGE`

31. Удалить все образы с с названием <none>:<none> `docker image prune`

32. Удалить все образы из которых не запущены контейнеры (в любом статусе) `docker image prune --all`

33. Удалить все образы с любым статусом без подтверждения `docker rmi -f $(docker images -aq)`, так же можно подставить в конструкцию $() любую фильтрацию и написать `-q`

34. Документация в терминале `docker help docker-command` или `docker docker-command --help`. Официальная документация по docker cli https://docs.docker.com/engine/reference/run/
