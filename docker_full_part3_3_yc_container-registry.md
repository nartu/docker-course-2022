## Размещение своего образа в Yandex Cloud container registry

1. [Создать](https://passport.yandex.ru/registration?retpath=https%3A%2F%2Fcloud.yandex.ru%2Fdocs%2Fcli%2Fquickstart&process_uuid=5c0477f0-d9c2-47fa-8ad4-e1fbd9776538) учётную запись в Яндекс либо зайти в существующую.

2. Зайти в [Облако](https://cloud.yandex.ru), согласиться создать Облако и директорию, перейти в [консоль](https://console.cloud.yandex.ru).

3. Установить Yandex CLI и инициировать его как написано в [инструкции](https://cloud.yandex.ru/docs/cli/quickstart).

4. OAuth токен нельзя показывать, он даёт управление от имени пользователя различным программам, у каждого приложения на устройстве свой токен. Его можно [отозвать](https://passport.yandex.ru/profile/access), а так же посмотреть список внешних программ. [Причины](https://yandex.ru/dev/id/doc/dg/oauth/reference/token-invalidate.html) отзыва токена не пользователем. Подробнее о токенах и аутентификации [тут](https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token).

5. Создать [платёжный аккаунт](https://console.cloud.yandex.ru/billing?section=accounts). Без этого нельзя пользоваться сервисами. При регистрации снимают 11 рублей. Если номер телефона и карта ранее не использовались для регистрации в Яндекс Облаке, 60 дней действует грант: 1000 рублей на Compute Cloud (виртуальные машины) и 3000 рублей на остальные сервисы. После истечения периода или суммы деньги списываться не будут пока пользователь не согласится явно.

6. [Цена](https://cloud.yandex.ru/docs/container-registry/pricing) на Container Registry 3рубля/ГБ/Месяц. [Документация](https://cloud.yandex.ru/docs/container-registry/quickstart/).

7. Создание хранилища образов `yc container registry create --name nodetest`, так же это можно сделать через веб-интерфейс [Консоль](https://console.cloud.yandex.ru)->Container Registry. Нужно запомнить полученный ID. Потом можно посмотреть командой `yc container registry list`

8. Залогиниться в Docker `yc container registry configure-docker`. Это специальный помощник от Яндекс, который автоматизирует аутентификацию хранилища, используя OAuth токен, который был введён пользователем командой `yc init` (п. 3). Есть и другие способы [аутентификации](https://cloud.yandex.ru/docs/container-registry/operations/authentication).

9. Переименуем наш образ `docker tag node_app cr.yandex/ID/node_app:v1`

10. Зальём образ на Яндекс `docker push cr.yandex/ID/node_app:v1`

11. Добавим тэг latest и тоже зальём, это не будет занимать дополнительное место в хранилище, т.к слои идентично. Тэг latest принято переписывать после появления новых версий, это делается вручную.
```
docker tag node_app cr.yandex/ID/node_app:latest
docker push cr.yandex/ID/node_app:latest
```

12. Посмотреть список имеющихся образов `yc container image list`

13. На любой машине после аутентификации yandex container registry в Docker можно вытащить образ из Яндекс `docker pull cr.yandex/ID/node_app:v1`

14. В файле docker-stack.yml заменим в image локальный образ на образ из приватного репозитория
```
app:
  image: cr.yandex/ID/node_app:latest
```
