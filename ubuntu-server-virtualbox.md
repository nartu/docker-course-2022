## Установка Ubuntu server на VirtualBox

1. https://ubuntu.com/download/server Manual installation

2. Создать. Выбрать нужные поля. Сеть должна быть Сетевой мост и интерфейс основной для хоста. Тогда ВМ будет в локальной (относительно роутера) сети как ещё один компьютер с адресом типа 192.168.1.XXX

3. Запустить. Попросят подключить диск, подключаем скаченный iso образ. Оставляем всё по умолчанию, когда спросят про SSH отвечаем Установить, указываем логин и пароль. Можно сразу указать подключить репозиторий Docker, когда будет меню

4. Выясняем ip с помощью команды `ifconfig`, но сначала надо установить net-tools `sudo apt install net-tools` или командой `ip addr`

5. Подключение (если linux, для Винды приложение putty) `ssh user@ip`, запросится пароль

6. Делаем ssh ключ. На хосте `ssh-keygen -t rsa -f svname` -f название ключа, любое, без этого параметра называется id_rsa

7. Теперь надо добавить содержимое файла svname.pub в ~/.ssh/authorized_keys на ВМ. Для этого есть встроенная команда, из хоста `ssh-copy-id -i ~/.ssh/svname user@ip`

8. На всякий случай делаем копию оригинальной конфигурации `cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig`

9. Отключить авторизацию по паролю можно в конфиге ssh сервера, на ВМ `vi /etc/ssh/sshd_config`.Там `PasswordAuthentication no`, ещё принято отключать авторизацию
root `PermitRootLogin` (на настоящем сервере для безопасности)

10. Перегружаем `sudo service sshd restart`, status - посмотреть состояние.

11. На хосте создаём конфиг чтобы подключаться было проще. `touch ~/.ssh/config` и там
```
Host svhome
        HostName 192.168.1.XXX
        User user
        Port 22
        IdentityFile ~/.ssh/svhome

```

12. Теперь подключаться можно просто `ssh svhome`, либо вместо IdentityFile утилита `ssh-add ~/.ssh/svhome`
