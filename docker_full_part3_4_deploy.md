## Деплой

1. Разворачивать будем [здесь](ubuntu-server-virtualbox.md). Docker и docker-compose здесь уже установлены. Соединение настроено по ssh, по умолчанию можно подключаться `ssh user@ip`, где user - имя пользователя на этом сервере, ip - ip сервера, внешний или локальный (например, 192.168.X.X)

2. Нужно создать переменные окружения (пароль для БД), авторизоваться в docker чтобы скачивать из приватного репозитория и запустить скрипт для docker-compose, который скачает нужные образы и развернёт проект.

3. На ВМ нам нужны только `env-set.sh` чтобы прописать переменные окружения и сам скрипт `docker-stack.yml`. Для удобства создадим директорию где будем их хранить и скачаем их в эту директорию.

4. На ВМ:
```
cd /home/USER
mkdir yodo
cd yodo
```

5. Файлы можно перекинуть командой scp, это копирование по `ssh source target`. На рабочей машине:
```
scp env-set.sh docker-stack.yml user@ip(or alias):/home/USER/yodo
```

7. Теперь нужно [аутентифицироваться](https://cloud.yandex.ru/docs/container-registry/operations/authentication) в Container Registry. Подойдёт способ с помощью [IAM-токена](https://cloud.yandex.ru/docs/iam/operations/iam-token/create-for-sa), он действителен 12 часов, потом нужно получить новый. На рабочей машине получаем токен: `yc iam create-token`. На ВМ:
```
docker login \
         --username iam \
         --password <IAM-токен> \
         cr.yandex
```

8. Запускаем проект `docker-compose -f docker-stack.yml up -d` по адресу http://ip-VM наш сайт должен работать.

9. Посмотреть запущенные контейнеры-сервисы `docker ps` или `docker-compose ps`
